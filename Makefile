CC=go build
SOURCES=dialogflow.go
LIBRARY=dialogflow.a

all: $(SOURCES) $(LIBRARY)

$(LIBRARY): $(SOURCES)
	go test -cover
	GOOS=linux $(CC) $(SOURCES)

install:
	GOOS=linux go install

clean:
	@rm -fv $(LIBRARY)
