// dialogflow is a DialogFlow webhook API client package.
//
// Its primary purpose is to easily work with DialogFlow webhook requests and responses
// for Google Assistant.
package dialogflow

import (
	"encoding/json"
	"log"
)

// RequestV1 represents a DialogFlow Version 1 Request object. Used to interpret what the client has asked for.
type RequestV1 struct {
	Result          ResultV1
	OriginalRequest OriginalRequestV1
}

// ResultV1 represents a DialogFlow Version 1 `result` object, within a RequestV1 object.
type ResultV1 struct {
	ResolvedQuery string
}

// OriginalRequestV1 represents a DialogFlow Version 1 `originalRequest` object, within a RequestV1 object.
type OriginalRequestV1 struct {
	Data GoogleAssistantRequestV1
}

// GoogleAssistantRequestV1 represents a Google Assistant Version 1 Request object. DialogFlow includes this object
// in the `originalRequest.data` object.
type GoogleAssistantRequestV1 struct {
	User GoogleAssistantUser
}

// GoogleAssistantUser represents a Google Assistant `user` object, within a Google Assistant Request object.
type GoogleAssistantUser struct {
	UserId string `json:"user_id"`
}

// ResponseV1 represents a DialogFlow Version 1 Response object. User to respond to a DialogFlow webhook request.
type ResponseV1 struct {
	Speech      string         `json:"speech"`
	DisplayText string         `json:"displayText"`
	Data        ResponseDataV1 `json:"data"`
	ContextOut  []string       `json:"contextOut"`
}

// ResponseDataV1 represents a DialogFlow Version 1 `data` object, within a ResponseV1.
type ResponseDataV1 struct {
	Google GoogleAssistantResponseV1 `json:"google"`
}

// GoogleAssistantResponseV1 represents a Google Assistant Version 1 response object. It is included in
// the DialogFlow response object's `data` object.
type GoogleAssistantResponseV1 struct {
	ExpectUserResponse bool `json:"expect_user_response"`
	IsSsml             bool `json:"is_ssml"`
}

// RequestV2 represents a DialogFlow Version 2 Request object. Used to interpret what the client has asked for.
type RequestV2 struct {
	QueryResult                 QueryResultV2
	OriginalDetectIntentRequest OriginalDetectIntentRequestV2
}

// QueryResultV2 represents a DialogFlow Version 2 `queryResult` object, within a RequestV2 object.
type QueryResultV2 struct {
	QueryText string
}

// OriginalDetectIntentRequestV2 represents a DialogFlow Version 2 `originalDetectIntentRequestV2` object, within a RequestV2 object.
type OriginalDetectIntentRequestV2 struct {
	Payload PayloadV2
}

type PayloadV2 struct {
	User GoogleAssistantUserV2
}

// GoogleAssistantUserV2 represents a Google Assistant `user` object, within a Google Assistant Request V2 object.
type GoogleAssistantUserV2 struct {
	UserId string `json:"userId"`
}

type ResponseV2 struct {
	Payload ResponsePayloadV2 `json:"payload"`
}

type ResponsePayloadV2 struct {
	Google GoogleAssistantResponseV2 `json:"google"`
}

type GoogleAssistantResponseV2 struct {
	ExpectUserResponse bool           `json:"expectUserResponse"`
	RichResponse       RichResponseV2 `json:"richResponse"`
}

type RichResponseV2 struct {
	Items []ItemV2 `json:"items"`
}

type ItemV2 struct {
	SimpleResponse SimpleResponseV2 `json:"simpleResponse"`
}

type SimpleResponseV2 struct {
	TextToSpeech string `json:"textToSpeech"`
}

// BuildResponse formats a string response into a ResponseV1 type.
func BuildResponse(response string) ResponseV1 {
	log.Printf("Debug: ++buildAssistantResponse()")
	defer log.Printf("Debug: --buildAssistantResponse()")

	var ar ResponseV1

	ar.Speech = response
	ar.DisplayText = response
	ar.ContextOut = make([]string, 0)

	return ar
}

// ToJsonString() formats a ResponseV1 type into a JSON string. Used to return to HTTP clients in the body.
func (ar ResponseV1) ToJsonString() string {
	log.Printf("Debug: ++ToJsonString()")
	defer log.Printf("Debug: --ToJsonString()")

	r, _ := json.Marshal(ar)

	return string(r)
}

// BuildResponseV2 formats a string response into a ResponseV2 type.
func BuildResponseV2(response string) ResponseV2 {
	log.Printf("Debug: ++buildAssistantResponseV2()")
	defer log.Printf("Debug: --buildAssistantResponseV2()")

	var ar ResponseV2

	ar.Payload.Google.ExpectUserResponse = false
	ar.Payload.Google.RichResponse.Items = make([]ItemV2, 1)
	ar.Payload.Google.RichResponse.Items[0].SimpleResponse.TextToSpeech = response

	return ar
}

// ToJsonString() formats a ResponseV2 type into a JSON string. Used to return to HTTP clients in the body.
func (ar ResponseV2) ToJsonString() string {
	log.Printf("Debug: ++ToJsonString()")
	defer log.Printf("Debug: --ToJsonString()")

	r, _ := json.Marshal(ar)

	return string(r)
}
