package dialogflow

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAssistantResponseToJsonString(t *testing.T) {
	ar1 := BuildResponse("All lines are operating a good service.")

	tables := []struct {
		in   ResponseV1
		want string
	}{
		{ar1, "{\"speech\":\"All lines are operating a good service.\",\"displayText\":\"All lines are operating a good service.\",\"data\":{\"google\":{\"expect_user_response\":false,\"is_ssml\":false}},\"contextOut\":[]}"},
	}

	for _, table := range tables {
		got := table.in.ToJsonString()
		assert.Equal(t, table.want, got)
	}
}
